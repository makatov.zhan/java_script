// alert, confirm, prompt 
// типы данных и переменные
// любой текст записывается в кавычках

// console.log('привет')
// выводит данные для собсвтенного пользования и разработчиков
// тип данных- категория информации
// делятся на две части: приметивные и ссылочные
// примитивные типы данных 
// namber: 1,20,49.35/6
// String: это текстили строки 'text, 'asd', 'alex'
// Boolean логический: true, false правдалож
// undefined- не поределено значение

// console.log(5 + 10)
// console.log(5 - 10)
// console.log(5 * 10)
// console.log(5 / 10)

// console.log(20 / 3)
// console.log(20 % 3) % означает остаток от деления

// строки или String или текст
// console.log(typeof 'test');
// // typeof- проверяет тип данных значение
// // если без скобок или кавычек покажет что запродукт числа или текст
// console.log('Zhan' + "Makatov");
// // + контетенация- или соединение строк

// console.log(5000 + '50');
// // при соединение числа и строки он делает преобразование типов в строку

// console.log('50' - 5);
// // если между значением всё кроме + значение высчитывается как числа

// console.log("50%" + 20);

// console.log('50%' - 20);
// // NaN- это не число, не может пребразовать в число

// console.log(isNaN('asd' - 123));
// // isNaN- проверка на NaN, усли true,то значение равно NaN, если false - то значение обычное число

// console.log(isNaN(123));

// // isFinite
// console.log(1 / 0);

// console.log(9 * 90000000000000000000000);
// // работает до определённого числа

// console.log('сумма двух чисел', 20 + 5);

// переменная- ячейка памяти, var, let, const

// как создать ячейку, var используют редко 
// var name; // создаётся переменная, инициализируется 
// name = 'Ivan' // присвоение, 
//     // второй способ:
// var name = 'Andre' // сщздание и присвоение
//     // = не означает равенство

// console.log(name)

// console.log(test);

// var test = 'test'
// var- создаётся глобально и мы не можем получить доступ если напишем конслоь после вар

// var config = 'тут может лежать парольили ещё что то';
// перезаписывает переменные.эти две причины по этому его не используют

// console.log(name);

// let name = 'test' если писать после выдаст ошибку.

// let name = 'test'
// name = 'asd'
// console.log(name);

// const name = 'test'
//     // name = 'asd'
// console.log(name);
// нельзя перезаписывать константу


// const discountPrice = price * discount;
// console.log(parseInt('20.5'));
//Int(integer)-целое
// float -вещественные числа,те.е с точкой дробь
// console.log(parseFloat('20.5'));
// const discount = '10%';
// const price = 5000;
// // snake_case- каждое новоеслово разделено _
// // camelCase- каждое новое слово с большой буквы
// const discountPrice = (price * parseInt(discount)) / 100;
// const finally_price = price - discountPrice
// console.log('цена с учётом скидки', finally_price);

// const number = 79;
// // const ten = number / 10;
// // console.log(parseInt(ten));
// // Math- математические данные.
// // Math.round-округляет по математическим правилам
// // Math.ceil- округляет в большуюсторону
// // Math.floor- округляет в менгьшую сторону
// // console.log(Math.round(5.1));
// const ten = Math.floor(number / 10);
// const ed = number % 10;
// console.log(ed, ten);

// const res = ten + ed * 10;
// console.log(res);

// const number = 23654
// const ten = number % 100
// console.log(ten);

const number = 8982
const ten = (number % 100)
const ed = Math.floor(ten / 10)
console.log(ed);